// function openHideMenu() {
//     var leftMenu = document.getElementsByClassName("left-menu");
//     leftMenu.classList.add("toggle-menu");
//     var rightColumn = document.getElementsByClassName("table-big-wrapper");
//     rightColumn.classList.add("toggle-table-big-wrapper");
// }
$(document).ready(function () {
    $(".btn-left-menu").click(function () {
        $(".left-menu").toggleClass('toggle-left-menu');
        $(".table-big-wrapper").toggleClass('toggle-table-big-wrapper');
    });
    $(".table-project tr").click(function () {
        $(".right-menu-wrapper").toggleClass('toggle-right-menu');
    });

    if ($(".date-wrapper").length > 0) {
        var bindDatePicker = function () {
            $(".custom-datepicker").datepicker({
                // monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
                //     'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
                //     'Октябрь', 'Ноябрь', 'Декабрь'],
                // dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                // firstDay: 1,
                dateFormat: 'dd.mm.yy',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                showOn: "button",
                buttonImage: "/bundles/frontend/images/svg/calendar.svg",
                buttonImageOnly: true,
                multidate: true
            })
                .on('focus')
                .click(function () {
                    $(this).datepicker('show');
                });
        };
        bindDatePicker();
    };
});